<?php
/**
 * Autoloads files when requested
 * 
 * @since  1.0.0
 * @param  string $class_name Name of the class being requested
 */
function my_class_file_autoloader( $class_name ) {

    /**
     * If the class being requested does not start with our prefix,
     * we know it's not one in our project
     */

    $file_name =  $class_name; // lowercase

    // Compile our path from the current location
    $file = dirname( __FILE__ ) . '/classes/class-'. $file_name .'.php';

    // If a file is found
    if ( file_exists( $file ) ) {
        // Then load it up!
        require( $file );
    }
}
spl_autoload_register( 'my_class_file_autoloader' );
?>