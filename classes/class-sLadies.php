<?php

class sLadies{
  protected $lid;
  protected $timeslots = array(''=>'Clear','01:00'=>'01:00 am', '01:30'=>'01:30 am','02:00'=>'02:00 am', '02:30'=>'02:30 am','03:00'=>'03:00 am','03:30'=>'03:30 am','04:00'=>'04:00 am','04:30'=>'04:30 am','05:00'=>'05:00 am','05:30'=>'05:30 am','06:00'=>'06:00 am','06:30'=>'06:30 am','07:00'=>'07:00 am',
    '07:30'=>'07:30 am',  '08:00'=>'08:00 am','08:30'=>'08:30 am','09:00'=>'09:00 am','09:30'=>'09:30 am','10:00'=>'10:00 am','10:30'=>'10:30 am',
    '11:00'=>'11:00 am','11:30'=>'11:30 am',  '12:00'=>'12:00 am','12:30'=>'12:30 am',  '13:00'=>'01:00  pm', '13:30'=>'01:30 pm','14:00'=>'02:00 pm',  '14:30'=>'02:30 pm','15:00'=>'03:00 pm','15:30'=>'03:30 pm','16:00'=>'04:00 pm','16:30'=>'04:30 pm','17:00'=>'05:00 pm','17:30'=>'05:30 pm','18:00'=>'06:00 pm','18:30'=>'06:30 pm','19:00'=>'07:00 pm','19:30'=>'07:30 pm','20:00'=>'08:00 pm',  '20:30'=>'08:30 pm','21:00'=>'09:00 pm','21:30'=>'09:30 pm','22:00'=>'10:00 pm','22:30'=>'10:30 pm','23:00'=>'11:00 pm','23:30'=>'11:30 pm',  '24:00'=>'12:00 pm');
  public function __construct($lady_id =''){
    global $wpdb;
    global $post;
    $this->post = $post;
    $this->db = $wpdb;
    $this->lid = $lady_id;
  }
  
  function load_ladies(){
      $sql = 'SELECT * FROM `'.$this->db->prefix.'posts` WHERE `post_type` = "ladies" AND `post_status`= "publish"';
      $res = $this->db->get_results($sql);
      $count = count($res);

    $l = '<div id="our-ladies-ladies" class="col-md-12 col-sm-12 ladies_searchresult" data-ladies="'.$count.'">';
      $l.='<div class="row">';
      $id =1;
      if(isset($_GET['filter']) && $_GET['filter'] =='escort'){
        foreach ($res as $key => $item) {

          if(get_post_meta($item->ID,'escort',true) == 'Yes'){
            $img = get_field('feature_image',$item->ID,true);                      
            if(empty($img)){
            $img = get_field('default_ladies_image','Options');
            }
            $l.= '<div class="col-md-3 col-sm-12 ladies_searchitem equalHeight" id="lady-'.$id.'" data-figure="'.get_post_meta($item->ID,'figure',true).'" data-hair="'.get_post_meta($item->ID,'hair',true).'" data-statute="'.get_post_meta($item->ID,'stature',true).'" data-bust="'.get_post_meta($item->ID,'bust_type',true).'" data-name="'.get_post_meta($item->ID,'display_name',true).'">';
            $l.= '<a href="'.get_permalink($item->ID).'"><img src="'.$img['sizes']['lady-thumbs'].'" alt="'.$img['title'].'" title="'.$img['title'].'" /></a>';
            $l.= '<p><a href="'.get_permalink($item->ID).'">'.get_post_meta($item->ID,'display_name',true).'</a></p>';
            $l.= '<p>'.get_post_meta($item->ID,'short_description',true).'</p>';
            $l.= '</div>';
            $id ++;
           }
        }
      }else{
        foreach ($res as $key => $item) {
           $img = get_field('feature_image',$item->ID,true);                      
            if(empty($img)){
            $img = get_field('default_ladies_image','Options');
            }
          $l.= '<div class="col-md-3 col-sm-3 ladies_searchitem equalHeightParent" id="lady-'.$id.'" data-figure="'.get_post_meta($item->ID,'figure',true).'" data-hair="'.get_post_meta($item->ID,'hair',true).'" data-statute="'.get_post_meta($item->ID,'stature',true).'" data-bust="'.get_post_meta($item->ID,'bust_type',true).'" data-name="'.get_post_meta($item->ID,'display_name',true).'">';
          $l.= '<a href="'.get_permalink($item->ID).'" ><img class="equalHeight" src="'.$img['sizes']['lady-thumbs'].'" alt="'.$img['title'].'" title="'.$img['title'].'" /></a>';
          $l.= '<p><a href="'.get_permalink($item->ID).'">'.get_post_meta($item->ID,'display_name',true).'</a></p>';
          $l.= '<p>'.get_post_meta($item->ID,'short_description',true).'</p>';
          $l.= '</div>';
          $id ++;
        }
      }
      $l.= '</div>';
    $l.= '</div>';
    return $l;
  }
  function lady_roster(){
    $date = date('Y-m-d');
    $days = array(1 => 'Monday', 2 => 'Tuesday',3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday',6 => 'Saturday', 7 => 'Sunday');
    $key = array_search(strftime("%A", strtotime($date)), $days)-1;
    $new_days = array_slice($days, $key);
    $cur_time = time();
    $week_start =  (date("w")==1)?strtotime("0 hours 0 seconds") : strtotime("last Monday",mktime(0,0,0,date("n",$cur_time),date("j",$cur_time),date("Y",$cur_time)));
    $week_end =  $week_start+6*24*60*60;
    $week_start = date('Y-m-d', $week_start);
    $weeks_ends = date('Y-m-d', $week_end);
    $sql ="SELECT * FROM `".$this->db->prefix."ladies_roster` WHERE `lady_id`= ".$this->lid;
    $res = $this->db->get_results($sql);
    $r = '<div class="col-md-6 col-sm-12">';
    $r.= '<p class="stiletto_title_red">Current Week</p>';
    $data = array();
    for($i = 0; $i < count($res); $i++){
      $data[$res[$i]->lady_id] = maybe_unserialize($res[$i]->data);
    }

    foreach ($data as $key => $value) {
      for($i = 0; $i < count($value); $i++){
        $ex = explode(',',$value[$i]);
        if(date('Y-m-d', strtotime($ex[0])) >= date('Y-m-d') && date('Y-m-d', strtotime($ex[0])) <= $weeks_ends){
          if($ex[1]!=''){
            $time = array_search($ex[1], $this->timeslots);
            $result = date('H:i',strtotime($time.' + '.$ex[2].' hours'));
            $r.= '<p class="single_lady_roster_days"><span class="single_lady_roster_day">'.date('l', strtotime($ex[0])).':</span><span class="single_lady_roster_time">'.$ex[1].'-'.$this->timeslots[$result].'</span></p>';
          }
          }
      }
    }
    $r.= '</div>';
    $r.= '<div class="col-md-6 col-sm-12">';
      $r.= '<p class="stiletto_title_red">Next Week</p>';
    foreach ($data as $key => $value) {
      for($i = 0; $i < count($value); $i++){
        $ex = explode(',',$value[$i]);
        if(date('Y-m-d', strtotime($ex[0])) > $weeks_ends){
          if($ex[1]!=''){
            $time = array_search($ex[1], $this->timeslots);
            $result = date('H:i',strtotime($time.' + '.$ex[2].' hours'));
            $r.= '<p class="single_lady_roster_days"><span class="single_lady_roster_day">'.date('l', strtotime($ex[0])).':</span><span class="single_lady_roster_time">'.$ex[1].'-'.$this->timeslots[$result].'</span></p>';
          }
        }
      }
    }
    $r.= '</div>';
    return $r;
  }
  function featured_ladies(){
 $sql = "SELECT t1.ID, t1.post_title FROM `d2wp_posts` AS t1, `d2wp_postmeta` AS t2
            WHERE t1.ID = t2.post_id AND t2.meta_key='active' and t2.meta_value='Yes' AND t1.post_type='ladies' AND t1.post_status = 'publish' ORDER BY t1.menu_order ASC";
   $res = $this->db->get_results($sql);
    $g = '<div class="col-md-12 col-sm-12 col-lg-12">';
      $g.= '<div class="als-container" id="slider3">';
        $g.= ' <div class="als-viewport">';
        $g.= '<span class="lady_gallery_featured_title stiletto_title_red">Featured</span>';
        $g.= '<span class="als-prev"><img src="'.plugin_dir_url( __FILE__ ).'images/arrows.png" alt="prev" title="previous" /></span>';
        $g.= ' <span class="als-next"><img src="'.plugin_dir_url( __FILE__ ).'/images/arrows.png" alt="next" title="next" /></span>';
          $g.= ' <ul class="als-wrapper">';
          foreach ($res as $key => $value) {
            if(get_post_meta($value->ID,'featured',true) =="Yes"){ 
              $img = get_field('feature_image',$value->ID,true);                      
              if(empty($img)){
              $img = get_field('default_ladies_image','Options');
              }
              $g.= '<li class="als-item "><a class="" href="'.get_permalink($value->ID).'"><img class="equalHeight img-responsive" src="'.$img['sizes']['lady-thumbs'].'" alt="'.$img['title'].'" title="'.$img['title'].'" /></a><span class="lady_gallery_name"><a href="'.get_permalink($value->ID).'">'.get_post_meta($value->ID,'display_name',true).'</a></span><span class="lady_gallery_description">'.get_post_meta($value->ID,'short_description',true).'</span></li>';

            }
          }
          $g.= '</ul>';
        $g.= '</div>';
      $g.= '</div>';
    $g.= '</div>';
    return $g;
  }
}

?>