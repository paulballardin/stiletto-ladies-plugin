<?php

class sRoster{
  protected $roster_id;
  protected $date;
  protected $days;
  protected $key;
  protected $new_days;
  protected $timeslots = array(''=>'Clear','00:00'=>'00:00 am','01:00'=>'01:00 am', '01:30'=>'01:30 am','02:00'=>'02:00 am', '02:30'=>'02:30 am','03:00'=>'03:00 am','03:30'=>'03:30 am','04:00'=>'04:00 am','04:30'=>'04:30 am','05:00'=>'05:00 am','05:30'=>'05:30 am','06:00'=>'06:00 am','06:30'=>'06:30 am','07:00'=>'07:00 am','07:30'=>'07:30 am',  '08:00'=>'08:00 am','08:30'=>'08:30 am','09:00'=>'09:00 am','09:30'=>'09:30 am','10:00'=>'10:00 am','10:30'=>'10:30 am','11:00'=>'11:00 am','11:30'=>'11:30 am',  '12:00'=>'12:00 pm','12:30'=>'12:30 am',  '13:00'=>'01:00  pm', '13:30'=>'01:30 pm','14:00'=>'02:00 pm','14:30'=>'02:30 pm','15:00'=>'03:00 pm','15:30'=>'03:30 pm','16:00'=>'04:00 pm','16:30'=>'04:30 pm','17:00'=>'05:00 pm','17:30'=>'05:30 pm','18:00'=>'06:00 pm','18:30'=>'06:30 pm','19:00'=>'07:00 pm','19:30'=>'07:30 pm','20:00'=>'08:00 pm',  '20:30'=>'08:30 pm','21:00'=>'09:00 pm','21:30'=>'09:30 pm','22:00'=>'10:00 pm','22:30'=>'10:30 pm','23:00'=>'11:00 pm','23:30'=>'11:30 pm',  '24:00'=>'00:00 am');
  public function __construct($roster_id=''){
    global $wpdb;
    global $post;
    $this->post = $post;
    $this->db = $wpdb;
    $this->roster_id = $roster_id;
    $this->date = date('Y-m-d');
    $this->days = array(1 => 'Monday', 2 => 'Tuesday',3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday',6 => 'Saturday', 7 => 'Sunday');
    $this->key = array_search(strftime("%A", strtotime($this->date)), $this->days)-1;
    $this->new_days = array_slice($this->days, $this->key);
  }
  function timeslots(){
    return $this->timeslots;
  }
  function print_hidden_fields(){
  }

  function print_roster_page(){
  }

  function schedule($ex1, $ex2){

    $sql ="SELECT * FROM `".$this->db->prefix."ladies_roster`";
    $res = $this->db->get_results($sql);
    $dt = date("Y-m-d");
    $date_to =  date( "Y-m-d", strtotime( "$dt +6 day" ));
    for($i = 0; $i < count($res); $i++){
     $pst = get_post($res[$i]->lady_id);
      if($pst->post_status == 'publish'){
       $data[$res[$i]->lady_id] =  $res[$i]->data;
       $data[$res[$i]->lady_id] = maybe_unserialize($res[$i]->data);
      }
    }
    $date_current = date('Y-m-d');
    $r = '';
    for($ij = 0; $ij < 7; $ij++){
      $r.='<div class="day_roster_days">';
      $r.= '<p><span>'.strftime("%A", strtotime(date('Y-m-d',strtotime("+".$ij." day", strtotime($date_current))))).'</span><br/>'.date('d/m/Y',strtotime("+".$ij." day", strtotime($date_current))).'</p>';
      if ($data) {
        foreach ($data as $key => $item) {
          for($i = 0; $i < count($item); $i++){
            $ex = explode(',',$item[$i]);
            if(date('Y-m-d', strtotime($ex[0])) === date('Y-m-d',strtotime("+".$ij." day", strtotime($date_current))) && $ex[1]){
              $time = array_search($ex[1], $this->timeslots);
              $result = date('H:i',strtotime($time.' + '.$ex[2].' hours'));
              $chasov = strtotime($ex[1]);
              if(date("G",$chasov) >=$ex1 && date("H",$chasov) <= $ex2){
                // if($ex[1] === '12:00 pm'){
                //   $ex[1] = '00:00 am';
                // }
                $r.= '<div class="lady_day_roster eq_height" data-figure="'.get_post_meta($key,'figure',true).'" data-hair="'.get_post_meta($key,'hair',true).'" data-statute="'.get_post_meta($key,'stature',true).'" data-bust="'.get_post_meta($key,'bust_type',true).'" data-name="'.get_post_meta($key,'display_name',true).'"><span><a href="'.get_permalink($key).'" title="Stiletto Roster">'.get_post_meta($key,'display_name',true).'</a></span><span>'.$ex[1].' - '.$this->timeslots[$result].'</span>';
                  $r.= '<div class="ladies_roster_preview">';
                    $img = (get_field('feature_image',$key,true));
                    if(empty($img)){
                      $img = get_field('default_ladies_image','Options');
                      }
                    $r.= '<div class="col-md-6 ladies_roster_preview_image">';
                      $r.= '<a href="'.get_permalink($key).'"><img class="img-responsive" src="'.$img['sizes']['lady-thumbs'].'" alt="'.$img['alt'].'" title="'.$img['title'].'"/></a>';
                    $r.= '</div>';

                    $r.= '<div class="col-md-6 ladies_roster_preview_info">';
                      $r.= '<p class="lady_preview_name"><a href="'.get_permalink($key).'">'.get_post_meta($key,'display_name',true).'</a></p>';
                      $r.= '<p class="lady_preview_description">'.get_post_meta($key,'short_description',true).'</p>';
                      $r.= '<p class="lady_info_options lady_info_statute"><span>Stature: </span>'.get_post_meta($key,'stature',true).'</p>';
                      $r.= '<p class="lady_info_options lady_info_figure"><span>Figure: </span>'.get_post_meta($key,'figure',true).'</p>';
                      $r.= '<p class="lady_info_options lady_info_hiar"><span>Hair: </span>'.get_post_meta($key,'hair',true).'</p>';
                    $r.= '</div>';
                  $r.= '</div>';
                $r.= '</div>';
              }
            }
          }
        }
      }
    $r.= '</div>';
    }
    return $r;
  }

  function current_roster(){
    $sql ="SELECT * FROM `".$this->db->prefix."ladies_roster`";
    $res = $this->db->get_results($sql);
    $r = '<p class="roster_home_page_ladies_title stiletto_title_red">Ladies Available Today</p>';
    $r.= '<div class="roster_content_wrapper">';
      if($res){
      for($i = 0; $i < count($res); $i++){
        $data[$res[$i]->lady_id] =  $res[$i]->data;
        $data[$res[$i]->lady_id] = maybe_unserialize($res[$i]->data);
      }
      foreach ($data as $key => $item) {
        $pst = get_post($key);
        if($pst->post_status == 'publish'){
          for($i = 0; $i < count($item); $i++){
            $ex = explode(',',$item[$i]);
            if(date('Y-m-d', strtotime($ex[0])) === date('Y-m-d') && $ex[1]){
              $time = array_search($ex[1], $this->timeslots);
              $result = date('H:i',strtotime($time.' + '.$ex[2].' hours'));
              $r.= '<div class="lady_roster_item">';
                $r.= '<span class="lady_roster_item_name">'.get_post_meta($key,'display_name',true).'</span>';
                $r.= '<span class="lady_roster_item_time">'.$ex[1].' to '.$this->timeslots[$result].'</span>';
                $r.= '<div class="ladies_roster_preview">';
                $img = (get_field('feature_image',$key,true));
                if(empty($img)){
                $img = get_field('default_ladies_image','Options');
                }
                $r.= '<div class="col-md-6 ladies_roster_preview_image">';
                  $r.= '<a href="'.get_permalink($key).'" title="Stiletto Roster"><img class="img-responsive" src="'.$img['sizes']['lady-thumbs'].'" alt="'.$img['alt'].' title="'.$img['title'].'"/></a>';
                $r.= '</div>';
                $r.= '<div class="col-md-6 ladies_roster_preview_info">';
                  $r.= '<p class="lady_preview_name"><a href="'.get_permalink($key).'" title="Stiletto Roster">'.get_post_meta($key,'display_name',true).'</a></p>';
                  $r.= '<p class="lady_preview_description">'.get_post_meta($key,'short_description',true).'</p>';
                  $r.= '<p class="lady_info_options lady_info_statute"><span>Stature: </span>'.get_post_meta($key,'stature',true).'</p>';
                  $r.= '<p class="lady_info_options lady_info_figure"><span>Figure: </span>'.get_post_meta($key,'figure',true).'</p>';
                  $r.= '<p class="lady_info_options lady_info_hiar"><span>Hair: </span>'.get_post_meta($key,'hair',true).'</p>';
                $r.= '</div>';
              $r.= '</div>';
              $r.= '</div>';
            }
          }
        }
      }
    }
    $r.= '</div>';
    return $r;
  }
}

?>