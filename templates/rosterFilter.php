<div id="rosterFilterDiv">
	<p class="stiletto_title_red">Find Your Lady</p>
	<p class="rfilter_stature">
		<span>by <b>Stature</b></span>
		<input type="checkbox" name="rfilter_stature" class="rFilterStature" value="" id="all_filter" checked/>
		<input type="checkbox" name="rfilter_stature" id="rfilterTall" class="rFilterStature" value="tall" /><label for="rfilterTall">tall</label>
		<input type="checkbox" name="rfilter_stature" id="rfilterMedium" class="rFilterStature" value="tmedium"/><label for="rfilterMedium">medium</label>
		<input type="checkbox" name="rfilter_stature" id="rfilterPetite" class="rFilterStature" value="petite"/><label for="rfilterPetite">petite</label>
	</p>
	<p class="rfilter_figure">
		<span>by <b>Figure</b></span>
		<input type="checkbox" name="rfilter_figure" class="rFilterFigure" value="" id="all_filter" checked/>
		<input type="checkbox" name="rfilter_figure" id="rfilterSlim" class="rFilterFigure" value="slim" /><label for="rfilterSliml">slim</label>
		<input type="checkbox" name="rfilter_figure" id="rfilterAthletic" class="rFilterFigure" value="athletic" /><label for="rfilterAthletic">athletic</label>
		<input type="checkbox" name="rfilter_figure" id="rfilterCurvy" class="rFilterFigure" value="curvy" /><label for="rfilterCurvy">curvy</label>
		<input type="checkbox" name="rfilter_figure" id="rfilterBusty" class="rFilterFigure" value="busty" /><label for="rfilterBusty">busty</label>
		<input type="checkbox" name="rfilter_figure" id="rfilterVoloptuous" class="rFilterFigure" value="voloptuou" /><label for="rfilterVoloptuous">voloptuous</label>
	</p>
	<p class="rfilter_hair">
		<span>by <b>Hair</b></span>
		<input type="checkbox" name="rfilter_hair" class="rFilterHair" value="" id="all_filter" checked/>
		<input type="checkbox" name="rfilter_hair" id="rfilterPlatinum" class="rFilterHair" value="latinum" /><label for="rfilterPlatinum">platinum</label>
		<input type="checkbox" name="rfilter_hair" id="rfilterBlonde" class="rFilterHair" value="blonde" /><label for="rfilterBlonde">blonde</label>
		<input type="checkbox" name="rfilter_hair" id="rfilterBrunette" class="rFilterHair" value="brunette" /><label for="rfilterBrunette">brunette</label>
		<input type="checkbox" name="rfilter_hair" id="rfilterAuburn" class="rFilterHair" value="auburn" /><label for="filterAuburn">auburn</label>
		<input type="checkbox" name="rfilter_hair" id="rfilterRedhead" class="rFilterHair" value="redhead" /><label for="rfilterRedhead">redhead</label>
	</p>
	<p class="rfilter_bust">
		<span>by <b>Bust</b></span>
		<input type="checkbox" name="rfilter_bust" class="rFilterBust" value="" id="all_filter" checked/>
		<input type="checkbox" name="rfilter_bust" id="rfilterNatural" class="rFilterBust" value="natural" /><label for="rfilterNatural">natural</label>
		<input type="checkbox" name="rfilter_bust" id="rfilterEnhanced" class="rFilterBust" value="enhanced" /><label for="rfilterEnhanced">enhanced</label>
	</p>
</div>