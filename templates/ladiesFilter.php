<div class="col-sm-3 ladies_filter">
		<div id="filterDiv">
			<input name="filter_by_name" class="lady_filter" id="filter_by_name" class="filter_by_name" value="" placeholder="Search by Name"/>
			<span class="filter_by-stature sp_filter closed"><strong>Filter</strong> by stature <i class="fa fa-angle-down" aria-hidden="true"></i></span>
			<div class="filter_by_statute_option ">
				<input type="checkbox" class="lady_filter statute_filter" name="statute" id="all_filter" checked="checked" value style="display: none;"/>
				<p>
					
					<input type="checkbox" class="lady_filter statute_filter" name="statute" id="stature_filter" value="tall"/>
					<label for="stature_filter">Tall</label> 
				</p>
				<p>
					<input type="checkbox"  class="lady_filter statute_filter" name="statute" id="medium_filter" value="medium"/>
					<label for="medium_filter">Medium</label> 
				</p>
				<p>
					<input type="checkbox" class="lady_filter statute_filter" name="statute" id="petite_filter" value="petite"/>
					<label for="petite_filter">Petite</label> 
				</p>
			</div>
			<span class="filter_by-hair sp_filter closed"><strong>Filter</strong> by hair <i class="fa fa-angle-down" aria-hidden="true"></i></span>

			<div class="filter_by_hair_option ">
				<input type="checkbox" class="lady_filter hair_filter" name="hair[1][]" id="all_filter" value checked="checked" style="display: none;"/>
				<p> 
					<input type="checkbox" class="lady_filter hair_filter" name="hair[1][]" id="blonde_filter" value="blonde"/>
					<label for="blonde_filter">Blonde</label>
				</p>
				<p>
					<input type="checkbox" class="lady_filter hair_filter"  name="hair[1][]" id="brunette_filter" value="brunette"/>
					<label for="brunette_filter">Brunette</label> 
				</p>
				<p>
					<input type="checkbox" class="lady_filter hair_filter"  name="hair[1][]" id="black_filter" value="black"/>
					<label for="black_filter">Black</label> 
				</p>
				<p>
					<input type="checkbox" class="lady_filter hair_filter"  name="hair[1][]" id="redhead_filter" value="redhead"/>
					<label for="redhead_filter">Redhead</label> 
				</p>
				<p>
					<input type="checkbox" class="lady_filter hair_filter"  name="hair[1][]" id="other_filter" value="other"/>
					<label for="other_filter">Other</label>
				</p>
			</div>
			<span class="filter_by-figure sp_filter closed"><strong>Filter</strong> by figure <i class="fa fa-angle-down" aria-hidden="true"></i></span>
			<div class="filter_by_figure_option">
					<p>
						<input type="checkbox" class="lady_filter figure_filter" name="figure[1][]" id="all_filter" value checked="checked" style="display: none;"/>
						<input type="checkbox" class="lady_filter figure_filter" name="figure[1][]" id="slim_filter" value="slim"/>
						<label for="slim_filter">Slim</label> 
					</p>
					<p>
						<input type="checkbox" class="lady_filter figure_filter"  name="figure[1][]" id="athletic_filter" value="athletic"/>
						<label for="athletic_filter">Athletic</label> 
					</p>
					<p>
						<input type="checkbox" class="lady_filter figure_filter"  name="figure[1][]" id="curvy_filter" value="curvy"/>
						<label for="curvy_filter">Curvy</label> 
					</p>
					<p>
						<input type="checkbox" class="lady_filter figure_filter"  name="figure[1][]" id="voluptuous_filter" value="voluptuous"/>
						<label for="voluptuous_filter">Voluptuous</label> 
					</p>
					<p>
						<input type="checkbox" class="lady_filter figure_filter"  name="figure[1][]" id="busty_filter" value="busty"/>
						<label for="busty_filter">Busty</label> 
				</p>
			</div>
			<span class="filter_by-bust sp_filter closed"><strong>Filter</strong> by bust <i class="fa fa-angle-down" aria-hidden="true"></i></span>
			<div class="filter_by_bust_option">
					<p>
						<input type="checkbox" class="lady_filter bust_filter" name="bust[1][]" id="all_filter" value checked="checked" style="display: none;"/>
						<input type="checkbox" class="lady_filter bust_filter" name="bust[1][]" id="natural_filter" value="natural"/>
						<label for="natural_filter">Natural</label> 
					</p>
					<p>
						<input type="checkbox" class="lady_filter bust_filter"  name="bust[1][]" id="enhanced_filter" value="enhanced"/>
						<label for="enhanced_filter">Enhanced</label> 
					</p>
			</div>
		</div>
</div>