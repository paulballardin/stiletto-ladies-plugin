var $ = jQuery.noConflict();
$(function(){
  $.getScript('http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js',function(){
    var select = $('#add_lady_roster').select2({
    formatNoMatches: function() {
        return 'No Ladies Found';
    },
    dropdownCssClass: 'select2-hidden'
});
    var select2 = $('.add_lady_roster_time').select2();
    var select3 = $('.add_lady_roster_time2').select2();
   
    
  }); 
});
$(document).ready(function(){
  $('.edit_lady-button').on('click',function(){
    jQuery.ajax({
      type:"POST",
      url: workscripts_ajx.ajax_url,
      data: {
        action: "wk_ajx",
        load_lady: 'load'
      },
      success:function(data){
        $('.roster_wrapper_hidden').html(data);
        $('#edit_lady_roster_select').select2().on("select2-selecting", function(e) {
          $('.hidden-rosters-forms_edit').slideDown(250);
             jQuery.ajax({
              type:"POST",
              url: workscripts_ajx.ajax_url,
              data: {
                action: "wk_ajx",
                load_lady_id: e.val
              },
              success:function(data){
             
                $('.roster_wrapper_hidden').append(data);
                $('.add_lady_roster_time').select2();
                $('.hidden-rosters-forms_edit').slideDown(500);
                $(document).find('.save_lady_button').on('click',function(){
                  save_data();
                });
                $(document).find('.remove_lady_button').on('click',function(){
                  remove_data();
                });
                $(document).find('.clear_lady_button').on('click',function(){
                  $('body').find('input.current_time').val('');
                  $('body').find('input.next_time').val('');
                  $('#add_lady_roster').select2();
                  $('.add_lady_roster_time').select2("val", "");
                  $('.add_lady_roster_time2').select2("val", "");
                  save_data();
                });
                
              },
              error: function(errorThrown){
              } 
          });
        });

        $('.roster_wrapper_hidden').slideDown(500);

      },
      error: function(errorThrown){
        //console.log(errorThrown);
      } 
  });

  });
  $('.ladies_table').DataTable();
  $('.add-lady_button').on('click',function(){
    $('.roster_wrapper_hidden').slideDown(500);
  });
  var select = $('#add_lady_roster');
  select.on("select2-selecting", function(e) {
        $('.hidden-rosters-forms').slideDown(250);
     })
$(document).find('.save_lady_button').on('click',function(){
  save_data();
});

function save_data(){
  var ldata = {};
  var mass = new Object;
  var data='';
  var lady = $('.save_lady_button').parents().find('select.add_lady_roster').val();
  var n = 0;
  var select =  $('.save_lady_button').parents().find('select.add_lady_roster_time').each(function(){
  var totalDuration = 0; 
    if($(this).attr('data_day') != 'undefined' && $(this).attr('data_day')){
      var day = $(this).attr('data_day');
      var time = $(this).val();
      var duration = $(this).parent().find('input.inpt_time').val();
      if (time === 'Clear' || time==='' || duration == 0){ time=''; duration = ''};
      data += day+','+time+','+duration+'|';
      totalDuration += duration;
      mass[lady]= data;
      n++;
    }
  });

    jQuery.ajax({
      type:"POST",
      url: workscripts_ajx.ajax_url,
      data: {
         action: "wk_ajx",
        lady_data: mass
      },
      success:function(data){
        $('.roster_wrapper_hidden').slideUp(500);
        $('body').find('input.current_time').val('');
        $('body').find('input.next_time').val('');
        $('#add_lady_roster').select2();
        $('.add_lady_roster_time').select2("val", "");
        $('.add_lady_roster_time2').select2("val", "");
        location.reload();
        // setInterval( function() {
        //   $('.settinds_saved').slideDown(500); 
        // } , 3000)
        // setInterval( function() {
        //   $('.settinds_saved').slideUp(500); 
        // } , 5000)
      },
      error: function(errorThrown){
        //console.log(errorThrown);
      } 
  });
}

function remove_data(){
  var lady = $('.remove_lady_button').parents().find('select.add_lady_roster').val();
    jQuery.ajax({
      type:"POST",
      url: workscripts_ajx.ajax_url,
      data: {
         action: "wk_ajx",
        lady_remove: lady
      },
      success:function(data){
        $('.roster_wrapper_hidden').slideUp(500);
        $('body').find('input.current_time').val('');
        $('body').find('input.next_time').val('');
        $('#add_lady_roster').select2();
        $('.add_lady_roster_time').select2("val", "");
        $('.add_lady_roster_time2').select2("val", "");
        console.log('deleted');
        location.reload();
        // setInterval( function() {
        //   $('.settinds_saved').slideDown(500); 
        // } , 3000)
        // setInterval( function() {
        //   $('.settinds_saved').slideUp(500); 
        // } , 5000)
      },
      error: function(errorThrown){
        console.log(errorThrown);
      } 
  });
}



});
