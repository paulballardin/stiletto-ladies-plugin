<?php
/*
Plugin Name: Stiletto Ladies
Plugin URI: http://outsmart.net.au
Description: Plugin for the Stiletto website
Version: 1.0
Author: paulballardin
Author URI: http://outsmart.net.au
*/

@require_once 'autoload.php';
add_action( 'admin_menu', 'register_roster_page' );
function add_theme_menu_item(){
	add_menu_page("Theme Options", "Theme Options", "manage_options", "theme-options", "theme_settings_page", null, 99);
}
add_action("admin_menu", "add_theme_menu_item");
function theme_settings_page()
{
    ?>
	    <div class="wrap">
	    <h1>Theme Options</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");
	            submit_button();
	        ?>
	    </form>
		</div>
	<?php
}
	function full_width(){
		?>
	    	<input type="text" name="full_width" id="adm_full_width" style="text-align: center;" value="<?php echo get_option('full_width'); ?>" /> +px
	    <?php
	}
	function display_twitter_element(){
	?>
    	<input type="text" name="twitter_url" id="adm_twitter_url" value="<?php echo get_option('twitter_url'); ?>" />
    <?php
	}
	function display_facebook_element(){
		?>
	    	<input type="text" name="facebook_url" id="adm_facebook_url" value="<?php echo get_option('facebook_url'); ?>" />
	    <?php
	}
	function footer_text(){
		?>
	    	<input type="text" name="footer_text" id="adm_footer_text" value="<?php echo get_option('footer_text'); ?>" />
	    <?php
	}
	function show_ladies_rates(){
		?>
	    	<input type="checkbox" name="show_ladies_rates" id="adm_show_rates" value="1" <?php (get_option('show_ladies_rates') == 1) ? $checked = 'checked': $checked =  ''; ?>  <?php echo $checked; ?> />
	    <?php
	}	
	function display_theme_panel_fields(){
		add_settings_section("section", "All Settings", null, "theme-options");
		add_settings_field("full_width", "Main Content  Width", "full_width", "theme-options", "section");
		add_settings_field("twitter_url", "Twitter Profile Url", "display_twitter_element", "theme-options", "section");
	 	add_settings_field("facebook_url", "Facebook Profile Url", "display_facebook_element", "theme-options", "section");
	 	add_settings_field("footer_text", "Fotter Bottom Text", "footer_text", "theme-options", "section");
	 	add_settings_field("show_ladies_rates", "Show Ladies Rates?", "show_ladies_rates", "theme-options", "section");
	    register_setting("section", "full_width");
	    register_setting("section", "twitter_url");
	    register_setting("section", "facebook_url");
	    register_setting("section", "footer_text");
	    register_setting("section", "show_ladies_rates");
	}

add_action("admin_init", "display_theme_panel_fields");
function mytheme_customize_register( $wp_customize ) {
   $wp_customize->add_setting( 'default-content-layout' , array(
	    'default'     => '1024px',
	    'transport'   => 'refresh',
		));
}
add_action( 'customize_register', 'mytheme_customize_register' );
function mytheme_customize_css()
{
    ?>
         <style type="text/css">
            .default-content-layout { max-width:<?php echo get_theme_mod('default-content-layout', get_option('full_width')).'px'; ?>; }
         </style>
    <?php
}
add_action( 'wp_head', 'mytheme_customize_css');
function register_options(){

  wp_register_script('jQueryUiDataTables', plugins_url('/js/jQueryUiDataTables.js',__FILE__), array('jquery'),false, true);
  wp_register_script('workscripts', plugins_url('/js/workscripts.js',__FILE__), array('jquery'));
  wp_register_script('jQueryDataTables', plugins_url('/js/jQueryDataTables.js',__FILE__), array('jquery'));
  wp_register_style( 'jQueryDataTables-css', plugins_url('/css/jQueryUi.css',__FILE__), array());
  wp_register_style( 'smoothness-css', plugins_url('/css/smoothness.css',__FILE__), array());
  wp_register_style( 'plugin-css', plugins_url('/css/styles.css',__FILE__), array());
  // wp_register_style( 'bootstrap-css','http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', array());

}
add_action('admin_init', 'register_options');

function site_scripts_load() {
  wp_enqueue_script( 'site-js', 'http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', array('jquery'), false );
  wp_enqueue_script( 'site-js', plugins_url('/js/site.js',__FILE__), array('jquery'), false );


}
add_action( 'wp_enqueue_scripts', 'site_scripts_load' );

function register_roster_page(){
global $page_hook_suffix;
	$page_hook_suffix = add_menu_page( 'Roster', 'Roster', 'manage_options', 'roster', 'roster_page','', 26 );
}


function load_admin_scripts($hook) {
	global $page_hook_suffix;
  if( $hook != $page_hook_suffix )
    return;
  wp_enqueue_script( 'jQueryUiDataTables' );
  wp_enqueue_script( 'jQueryDataTables' );
  wp_enqueue_script( 'workscripts' );
  wp_localize_script( 'workscripts', 'workscripts_ajx', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
	));
  wp_enqueue_style( 'jQueryDataTables-css' );
  wp_enqueue_style( 'smoothness-css' );
  wp_enqueue_style( 'plugin-css' );
  wp_enqueue_style( 'bootstrap-css' );

}
add_action( 'wp_ajax_nopriv_wk_ajx', 'wk_ajx' );
add_action( 'wp_ajax_wk_ajx', 'wk_ajx' );
global $lID;
function wk_ajx(){
	if(isset($_POST['load_all']) ){
		echo 'sfsdfdsf';
		die();
	}

	if(isset($_POST['lady_data']) ){
		global $wpdb;

		$data =  $_POST['lady_data'];
		foreach ($data as $key => $value) {
			$ldata= substr($value, 0, -1);
			$ldata = explode('|', $ldata);
			$id = $key;
		}

		$arr[$id]= $ldata ;
		$arr = maybe_serialize( $arr[$id] );
		echo $arr;
		$sql = "SELECT `id` FROM `".$wpdb->prefix."ladies_roster`  WHERE `lady_id`=".$id;
		$res = $wpdb->get_results($sql);
		if($res){
		$wpdb->update(
				$wpdb->prefix.'ladies_roster',
				array(
					'lady_id' => $id,
					'data' => $arr
				),
				array( 'lady_id' => $id ),
				array(
					'%d',
					'%s'
				),
				array( '%d' )
			);
		}else{
			$wpdb->insert(
					$wpdb->prefix.'ladies_roster',
					array( 'lady_id' => $id, 'data' =>$arr),
					array( '%d', '%s' )
				);
		}
		$wpdb->show_errors();
		echo $wpdb->last_query;

		die();
	}
	if(isset($_POST['load_lady'])){
	global $wpdb;
	$sql = "SELECT t1.ID, t1.post_title FROM `d2wp_posts` AS t1, `d2wp_postmeta` AS t2
					WHERE t1.ID = t2.post_id AND t2.meta_key='active' and t2.meta_value='Yes' AND t1.post_type='ladies'";
	$ladies = $wpdb->get_results($sql);
		$h1.= '<div class="col-md-2 col-sm-4">';
				$h1.='<span>Select Lady</span>';
				$h1.= '<select class="form-control add_lady_roster" id="edit_lady_roster_select" data-placeholder="Select Lady">';
					$h1.='<option></option>';
					foreach ($ladies as $value ) {
						$sql1 ="SELECT `lady_id` FROM `".$wpdb->prefix."ladies_roster` WHERE `lady_id`= ".$value->ID;
						$res1 = $wpdb->get_col($sql1);
						if($res1[0] !=''){
							$h1.= ' <option value="'.$value->ID.'">'.$value->post_title.'</option>';
						}else{

						}

					}
				$h1.= '</select>';
			$h1.= '</div>';

				echo $h1;
		die();
	}
	if(isset($_POST['lady_remove'])){
	global $wpdb;
	$wpdb->delete( $wpdb->prefix.'ladies_roster', array( 'lady_id' => $_POST['lady_remove'] ) );
		die();
	}
	if(isset($_POST['load_lady_id'])){
		$roster = new sRoster();
		$timeslots = $roster->timeslots();

		global $wpdb;
		$date = date('Y-m-d');
		$days = array(1 => 'Monday', 2 => 'Tuesday',3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday',6 => 'Saturday', 7 => 'Sunday');
		$key = array_search(strftime("%A", strtotime($date)), $days)-1;
		$new_days = array_slice($days, $key);
		 $cur_time = time();
		 $week_start =  (date("w")==1)?strtotime("0 hours 0 seconds") : strtotime("last Monday",mktime(0,0,0,date("n",$cur_time),date("j",$cur_time),date("Y",$cur_time)));
		 $week_end =  $week_start+6*24*60*60;
		 $week_start = date('Y-m-d', $week_start);
		 $weeks_ends = date('Y-m-d', $week_end);
			$sql ="SELECT * FROM `".$wpdb->prefix."ladies_roster` WHERE `lady_id`= ".$_POST['load_lady_id'];
			$res = $wpdb->get_results($sql);
			if($res){
				for($i = 0; $i < count($res); $i++){
						$data[$res[$i]->lady_id] =  $res[$i]->data;
						$data[$res[$i]->lady_id] = maybe_unserialize($res[$i]->data);
				}

			}
		$h1.= '<div class="col-md-12 col-sm-12 hidden-rosters-forms hidden-rosters-forms_edit">';
			$h1.= '<form id="roster-inputs">';
				$h1.= '<div class="current lady_roster">';
					$h1.="<h3>Current Week</h3>";
						$q= 1;
						$d = 0;
						foreach ($data as $key => $value) {
							for($i = 0; $i < count($value); $i++){
									$ex = explode(',',$value[$i]);
									if(date('Y-m-d', strtotime($ex[0])) >= $week_start && date('Y-m-d', strtotime($ex[0])) <= $weeks_ends){
										if(date('Y-m-d', strtotime($ex[0])) < date('Y-m-d')){
											$display = 'style="display:none;"';
										}else{
											$display = '';
										}
											$h1.= '<p '.$display.'><span class="rosters_forms_day_names">'.$days[$q].'</span>';
													$h1.= '<select  class="add_lady_roster_time add_lady_roster_time_current ajx_load_options" id="add_lady_roster_time" data-placeholder="Select Time" data_day="'.$ex[0].'">';
														$h1.= '<option></option>';
														foreach ($timeslots as $key => $value2) {
															if($ex[1]===$value2){
																$s = 'selected';
															}else{
																$s = '';
															}
															$h1.='<option '.$s.' value="'.$value2.'">'.$value2.'</option>';
														}
												$h1.='</select><input type="number" class="current_time inpt_time" data_date="" value="'.$ex[2].'" time value="" placeholder="Time" max="24" min ="0"><btn ';
											$h1.='</p>';
										$d++;
										$q++;
									}

								}
							}

				$h1.='</div>';

				$h1.= '<div class="next lady_roster">';
					$h1.="<h3>Next Week</h3>";
					$j= 7;

					for($i = 1; $i <= count($days);$i++){
						foreach ($data as $key => $value) {
							$s1 = 1;
							for($i = 0; $i < count($value); $i++){
									$ex = explode(',',$value[$i]);
									if(date('Y-m-d', strtotime($ex[0])) > $weeks_ends){
										$h1.= '<p> <span class="rosters_forms_day_names">'.$days[$s1].'</span>';
										$h1.= '<select class="add_lady_roster_time add_lady_roster_time_next ajx_load_options" id="add_lady_roster_time2" data-placeholder="Select Time" data_day="'.date("Y-m-d", strtotime("+".$s1." day",strtotime($weeks_ends))).'">';
										$h1.= '<option></option>';
														foreach ($timeslots as $key => $value2) {
															if($ex[1]===$value2){
																$s = 'selected';
															}else{
																$s = '';
															}
															$h1.='<option '.$s.' value="'.$value2.'">'.$value2.'</option>';
														}
										$h1.='</select><input type="number" class="current_time inpt_time" data_date="" value="'.$ex[2].'" time value="" placeholder="Time" max="24" min ="0">';
										$h1.='</p>';
										$s1++;
										$next = true;
									}else{
										$next = false;
									}
								}
							}

				 		$j++;
				 	}
				 	if(!$next){
				 		echo 'next false';
				 		for($ij = 1; $ij <= count($days);$ij++){
				 			$h1.= '<p> <span class="rosters_forms_day_names">'.$days[$ij].'</span>';
				 				$h1.= '<select class="add_lady_roster_time add_lady_roster_time_next ajx_load_options" id="add_lady_roster_time2" data-placeholder="Select Time" data_day="'.date("Y-m-d", strtotime("+".$ij." day",strtotime($weeks_ends))).'">';
								$h1.= '<option></option>';
									foreach ($timeslots as $key => $value2) {
										$h1.='<option  value="'.$value2.'">'.$value2.'</option>';
									}
								$h1.='</select><input type="number" class="current_time inpt_time" data_date="" value=" " time value="" placeholder="Time" max="24" min ="1">';
				 			$h1.='</p>';
				 		}
				 	}
				 	$h1.= '</form>';
				 	$h1.= '</div>';
				 	$h1.= '<div class="col-md-12 col-sm-12">';
						$h1.= '<span class="save_lady_button save_lady_button2">Save</span><span class="clear_lady_button btn-warning">Clear</span><span class="remove_lady_button btn-danger btn-red">Remove</span>';
						$h1.='<div style="clear:both;"></div>';
					$h1.= '</div>';
				 	echo $h1;
		die();
	}

}


add_action('admin_enqueue_scripts', 'load_admin_scripts');
add_action('init', 'ladies_post_type');
add_action('init', 'ladies_database');
function ladies_database(){
	global $wpdb;
	$sql="CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ladies_roster(
				id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				lady_id INT(6),
				data VARCHAR(255) NOT NULL
				)";
	$res= $wpdb->get_results($sql);
}
function ladies_post_type()
{

  $labels = array(
		'name' => 'Ladies',
		'singular_name' => 'Lady',
		'add_new' => 'Add Lady',
		'add_new_item' => 'Add new Lady',
		'edit_item' => 'Edit Lady',
		'new_item' => 'New Lady',
		'view_item' => 'View Lady',
		'search_items' => 'Search Lady',
		'not_found' =>  'Not found',
		'not_found_in_trash' => 'Not found',
		'parent_item_colon' => '',
		'menu_name' => 'Ladies'
  );
  $args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','page-attributes')
  );
  register_post_type('ladies',$args);
}
function my_enqueue($hook) {
    if( 'edit.php' != $hook )
        return;
    wp_enqueue_script( 'my_custom_script', 'https://cdn.datatables.net/1.10.11/js/dataTables.jqueryui.min.js');
}
add_action( 'admin_enqueue_scripts', 'my_enqueue' );
add_filter( 'manage_edit-ladies_columns', 'edit_ladies_columns' ) ;

function edit_ladies_columns( $columns ) {

	$columns = array(
		// 'cb' => '',
		'id' => __( 'ID' ),
		'title' => __( 'Name' ),
		'display_name' => __( 'Display Name' ),
		'active' => __( 'Active' ),
		'featured' => __( 'Featured' ),
		'escort' => __( 'Escort' ),
		'featured_escort' => __( 'Featured Escort' ),
		'date' => __( 'Date Added' )
	);

	return $columns;
}
add_action( 'manage_ladies_posts_custom_column', 'manage_ladies_columns', 10, 2 );
function manage_ladies_columns( $column, $post_id ) {
	global $post;
	switch( $column ) {
		case 'id' :
			_e( $post->ID );
			break;
		case 'display_name' :
		_e( get_post_meta($post->ID,'display_name',true ) );
			break;
		case 'active' :
			$active = get_post_meta($post->ID,'active',true );
			if($active === "Yes"){
				_e('<input type="checkbox"  checked disabled/>');
			}else{
				_e('<input type="checkbox"  disabled/>');
			}
		break;
		case 'featured' :
			$featured = get_post_meta($post->ID,'featured',true );
			if($featured === "Yes"){
				_e('<input type="checkbox"  checked disabled/>');
			}else{
				_e('<input type="checkbox"  disabled/>');
			}
		break;
		case 'escort' :
			$escort = get_post_meta($post->ID,'escort',true );
			if($escort === "Yes"){
				_e('<input type="checkbox"  checked disabled/>');
			}else{
				_e('<input type="checkbox"  disabled/>');
			}
		break;
		case 'featured_escort' :
			$featured_escort = get_post_meta($post->ID,'featured_escort',true );
			if($featured_escort === "Yes"){
				_e('<input type="checkbox"  checked disabled/>');
			}else{
				_e('<input type="checkbox"  disabled/>');
			}
		break;
		default :
		break;
	}
}
add_filter( 'manage_edit-ladies_sortable_columns', 'ladies_sortable_columns' );
function ladies_sortable_columns( $columns ) {
	$columns['id'] = 'ID';
	$columns['display_name'] = 'Display Name';
	return $columns;
}

function roster_page(){
	$roster = new sRoster();
	$timeslots = $roster->timeslots();
	global $wpdb;
	$date = date('Y-m-d');
	$days = array(1 => 'Monday', 2 => 'Tuesday',3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday',6 => 'Saturday', 7 => 'Sunday');
	$key = array_search(strftime("%A", strtotime($date)), $days)-1;
	$new_days = array_slice($days, $key);
	$sql = "SELECT t1.ID, t1.post_title FROM `d2wp_posts` AS t1, `d2wp_postmeta` AS t2
					WHERE t1.ID = t2.post_id AND t2.meta_key='active' and t2.meta_value='Yes' AND t1.post_type='ladies'";
	$ladies = $wpdb->get_results($sql);
 	global $lID;
	$h = '<div class="col-md-12 col-sm-12">';
		$h.= "<div class='page-header'><h1>Roster</h1></div>";
		$h.= '<span class="add-lady_button">Add Roster</span>';
		$h.= '<span class=" edit_lady-button">Edit Roster</span>';
		$h.= '<p class="settinds_saved">Settings Saved</p>';
		$h.= '<div class="roster_wrapper roster_wrapper_hidden">';
			$h.= '<div class="col-md-2 col-sm-4">';
				$h.='<span>Select Lady</span>';
				$h.= '<select class="form-control add_lady_roster" id="add_lady_roster" data-placeholder="Select Lady">';
					$h.='<option></option>';
					foreach ($ladies as $value) {
						$sql ="SELECT * FROM `".$wpdb->prefix."ladies_roster` WHERE `lady_id`= ".$value->ID;
						$res = $wpdb->get_results($sql);
						if(!$res){
							$h.= ' <option value="'.$value->ID.'">'.$value->post_title.'</option>';
						}
					}
				$h.= '</select>';
			$h.= '</div>';
			$cur_time = time();
		 $week_start =  (date("w")==1)?strtotime("0 hours 0 seconds") : strtotime("last Monday",mktime(0,0,0,date("n",$cur_time),date("j",$cur_time),date("Y",$cur_time)));
		  $week_start = date('Y-m-d', $week_start);
			/*Hidden rosters forms*/
			$h.= '<div class="col-md-12 col-sm-12 hidden-rosters-forms">';
				$h.= '<div class="current lady_roster">';
					$h.="<h3>Current Week</h3>";
					$d= 0;
					for($i = 1; $i<=count($days);$i++){
						if(date("Y-m-d", strtotime("+".$d." day",strtotime($week_start))) < date('Y-m-d')){
								$display = 'style="display:none;"';
						}else{
							$display = '';
						}
				 		$h.= '<p '.$display.'> <span class="rosters_forms_day_names">'.$days[$i].'</span>';
				 			$h.= '<select class="add_lady_roster_time add_lady_roster_time_current" id="add_lady_roster_time" data-placeholder="Select Time" data_day="'. date("Y-m-d", strtotime("+".$d." day",strtotime($week_start))).'">';
								$h.='<option data-day="none" selected></option>';
								foreach ($timeslots as $key => $value) {
									$h.='<option value="'.$value.'">'.$value.'</option>';
								}
							$h.='</select><input type="number" class="current_time inpt_time" data_date="" time value="" placeholder="Time" max="24" min ="1">';
				 		$h.='</p>';
				 		$d++;
				 	}
				$h.='</div>';

				$h.= '<div class="next lady_roster">';
					$h.="<h3>Next Week</h3>";
					$j= 7;
					for($i = 1; $i <= count($days);$i++){
				 			$h.= '<p> <span class="rosters_forms_day_names">'.$days[$i].'</span>';
				 			$h.= '<select class="add_lady_roster_time add_lady_roster_time_next" id="add_lady_roster_time2" data-placeholder="Select Time"data_day="'.date ("Y-m-d", time() - ( -$j + date("N")-1) * 24*60*60).'">';
								$h.='<option data-day="none"></option>';
								foreach ($timeslots as $key => $value) {
									$h.='<option  value="'.$value.'">'.$value.'</option>';
								}
							$h.='</select><input type="number" class="next_time inpt_time" data_date="" min="1" max ="24" time  value="" placeholder="Time">';
				 		$h.='</p>';
				 		$j++;
				 	}
				$h.='</div>';
				$h.='<div style="clear:both;"></div>';
			$h.='</div>';
			/* END Hidden rosters forms*/
			$h.= '<div class="col-md-12 col-sm-12">';
				$h.= '<span class="save_lady_button">Save</span>';
				$h.='<div style="clear:both;"></div>';
			$h.= '</div>';
		$h.= '</div>';
		$h.= '<div style="clear:both;"></div>';
		$h.= '<hr/>';
	$h.= '</div>';
	$h.= '<div class="roster_wrapper">';
		$h.= '<div class="col-lg-12 col-md-12 col-sm-12">';
			$h.='<h3>Current week</h3>';
				$h.= '<table id="ladies_table" class="ladies_table" class="display" cellspacing="0" width="100%">';
					$h.= '<thead>';
						$h.= '<tr>';
							$h.= '<th>Lady</th>';
							for($i = 0; $i<count($new_days);$i++){
				 				$h.= '<th>'.$new_days[$i].'</th>';
				 			}
						$h.= '</tr>';
					$h.= '</thead>';
					$h.= '<tfoot>';
						$h.= '<tr>';
							$h.= '<th>Lady</th>';
							for($i = 0; $i<count($new_days);$i++){
				 				$h.= '<th>'.$new_days[$i].'</th>';
				 			}
						$h.= '</tr>';
					$h.= '</tfoot>';
					$h.= '<tbody>';
					 $cur_time = time();
					 $week_start =  (date("w")==1)?strtotime("0 hours 0 seconds") : strtotime("last Monday",mktime(0,0,0,date("n",$cur_time),date("j",$cur_time),date("Y",$cur_time)));
					 $week_end =  $week_start+6*24*60*60;
					 $weeks_ends = date('Y-m-d', $week_end);
						$sql ="SELECT * FROM `".$wpdb->prefix."ladies_roster`";
						$res = $wpdb->get_results($sql);
						if($res){
							for($i = 0; $i < count($res); $i++){
									$data[$res[$i]->lady_id] =  $res[$i]->data;
									$data[$res[$i]->lady_id] = maybe_unserialize($res[$i]->data);

							}
							foreach ($data as $key => $value) {

								//work out if there is data
								$r = '';
								for($i = 0; $i < count($value); $i++){
										$ex = explode(',',$value[$i]);
										if(date('Y-m-d', strtotime($ex[0])) >= date('Y-m-d') && date('Y-m-d', strtotime($ex[0])) <= $weeks_ends){
											if($ex[1]!=='' && $ex[2]!==0){
												$r.='<td data_date="'.$ex[0].'" style="text-align: center;"><span> '.$ex[1].'<br/> '.$ex[2].' hour(s)</span></td>';
											}else{
												$r.='<td data_date="'.$ex[0].'" style="text-align: center;"><span> Free </span></td>';
											}
										}
									}
								
								if ($r !== '') {
								$h.='<tr>';
									$h.='<td>'.get_post_meta($key,'display_name',true).'</td>';
									$h.=$r;
								$h.='</tr>';
								}
							}
						}
					$h.= '</tbody>';
				$h.= '</table>';
			$h.= '</div>';
			$h.= '<div class="col-md-12 col-lg-12 col-sm-12">';
				$h.='<h3>Next week</h3>';
				$h.= '<table id="ladies_table" class="ladies_table" class="display" cellspacing="0" width="100%">';
					$h.= '<thead>';
						$h.= '<tr>';
							$h.= '<th>Lady</th>';
							for($i = 1; $i <= count($days);$i++){
				 				$h.= '<th>'.$days[$i].'</th>';
				 			}
						$h.= '</tr>';
					$h.= '</thead>';
					$h.= '<tfoot>';
						$h.= '<tr>';
							$h.= '<th>Lady</th>';
							for($i = 1; $i <= count($days);$i++){
				 				$h.= '<th>'.$days[$i].'</th>';
				 			}
						$h.= '</tr>';
					$h.= '</tfoot>';
					$h.= '<tbody>';
						if($res){
							foreach ($data as $key => $value) {
								for($i = 0; $i < count($value); $i++){
									$ex1 = explode(',',$value[$i]);
									if($ex1[0] >  $weeks_ends) {
										$check[$key][$i] = $ex1[0].','.$ex1[1].','.$ex1[2];
									}
								}
							}
								if($check){
									foreach ($check as $key => $value) {
										$h.='<tr>';
											$h.='<td>'.get_post_meta($key,'display_name',true).'</td>';
											$value =  array_values($value);
											for($i = 0; $i < count($value); $i++){
													$ex1 = explode(',',$value[$i]);
													if($ex1[1]!=='' && $ex1[2]!==0){
													$h.='<td data_date="'.$ex1[0].'" style="text-align: center;"><span> '.$ex1[1].'<br/> '.$ex1[2].' hours<span></td>';
												}else{
													$h.='<td data_date="'.$ex1[0].'" style="text-align: center;"><span> Free <span></td>';
												}
											}
											if(count($value) < 7){
												$a = 7 - (int)count($value);
												for($i = 0; $i < $a; $i++){
													$h.='<td></td>';
												}
											}
										$h.='</tr>';
									}
								}
						}
					$h.= '</tbody>';
				$h.= '</table>';
			$h.= '</div>';
			$h.= '<div style="clear:both;"></div>';
	$h.= '</div>';
	echo $h;
}

function  featured_ladies(){
	$lady = new sLadies();
	echo $lady->featured_ladies();
}
add_shortcode('featured_ladies','featured_ladies');
function roster_current(){
	$roster = new sRoster();
	$r = $roster->current_roster();
	return $r;
}
add_shortcode('roster_current','roster_current');

function all_ladies(){
	require_once('templates/ladiesFilter.php');
	global $wpdb;
	$ladies  = new sLadies();
	echo $ladies->load_ladies();

}
add_shortcode('all_ladies','all_ladies');


function roster_filter(){
	require_once('templates/rosterFilter.php');
}
add_shortcode('roster_filter','roster_filter');

function single_lady_roster($atts){
	$lady = new sLadies($atts['id']);
	echo $lady->lady_roster();
}
add_shortcode('single_lady_roster','single_lady_roster');

function full_roster(){
	$roster = new sRoster();
	 $r = '<div class="day_shedule row">';
		 $r.= '<div class="day_roster_wrapper ">';
			$r.= '<p class="stiletto_title_red">Day Schedule</p>';
				$r.= $roster->schedule(0,12);
		$r.= '</div>';
	$r.= '</div>';
	 $r.= '<div class="night_shedule row">';
		$r.= '<p class="stiletto_title_red last">Night Schedule</p>';
				$r.= $roster->schedule(12,24);
		$r.= '</div>';
	$r.= '</div>';
	return $r;
}
add_shortcode('full_roster','full_roster');

?>
